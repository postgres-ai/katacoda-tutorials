Database Lab aims to boost software development and testing processes via
enabling ultra-fast provisioning of multi-terabyte databases.

Our steps:

1. prepare a machine with ZFS,
1. generate some PostgreSQL database for testing purposes,
1. prepare at least one snapshot to be used for cloning,
1. configure and launch the Database Lab server,
1. setup client CLI,
1. start using its API and client CLI for the fast cloning
of the Postgres database,
1. and, finally, consider benefits and power of thin clones.

If you want to use any cloud platform (like AWS or GCP) or run your Database Lab
on VMWare, or on bare metal, only the first step will slightly differ.
In general, the overall procedure will be pretty much the same.

[[_TOC_]]

## Step 1. Prepare a Linux machine with Docker and ZFS
Install dependencies:
```bash
sudo apt-get update && sudo apt-get install -y \
  apt-transport-https \
  ca-certificates \
  curl \
  jq \
  gnupg-agent \
  postgresql-client \
  software-properties-common \
  zfsutils-linux
```

Install Docker:
```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable"

sudo apt-get update && sudo apt-get install -y \
  docker-ce \
  docker-ce-cli \
  containerd.io
```

Manage Docker as a non-root user:
```bash
sudo groupadd docker

sudo usermod -aG docker $USER

newgrp docker 
```

Create a regular file to mock ZFS:
```bash
truncate --size 10GB disk
```

Now it is time to create a ZFS pool. 
```bash
sudo zpool create -f \
  -O compression=on \
  -O atime=off \
  -O recordsize=8k \
  -O logbias=throughput \
  -m /var/lib/dblab/data \
  dblab_pool \
  "$(pwd)/disk"
```

And check the result:
```bash
zfs list
df -hT
```

## Step 2. Generate an example database for testing purposes
Let's generate some synthetic database with data directory ("PGDATA") located at `/var/lib/dblab/data`. 

We will use a standard PostgreSQL tool called `pgbench`. 

To generate PGDATA with `pgbench`, we are going to launch a regular Docker container with Postgres temporarily. 

```bash
docker run \
  --name dblab_pg_initdb \
  --label dblab_sync \
  --env PGDATA=/var/lib/postgresql/pgdata \
  --env POSTGRES_HOST_AUTH_METHOD=trust \
  --volume /var/lib/dblab/data:/var/lib/postgresql/pgdata \
  --detach \
  postgres:12-alpine
```

Create the `workshop` database:
```bash
docker exec -it dblab_pg_initdb psql -U postgres \
  -c 'create database workshop'
```

Generate data in the `workshop` database using `pgbench`. With scale factor `-s 100`, the database size will be ~1.4 GiB:
```bash
docker exec -it dblab_pg_initdb pgbench -U postgres \
  -i -s 100 workshop
```

PGDATA is ready and now it is time to stop the temporary container with Postgres:
```bash
docker stop dblab_pg_initdb
```

## Step 3. Prepare the first snapshot
Create the first snapshot for our database:

```bash
export DATA_STATE_AT="$(TZ=UTC date '+%Y%m%d%H%M%S')"
sudo zfs snapshot dblab_pool@initdb
sudo zfs set dblab:datastateat="${DATA_STATE_AT}" dblab_pool@initdb
```

Here, we put the current data and time to `DATA_STATE_AT`, which is stored in ZFS snapshot meta data. 

This timestamp needs to correspond to a moment in time that represents the latest state of the database. 

> In a real-life case, you may need to promote your Postgres database when preparing a snapshot if it was in a "replica" state. 
>
> See [snapshot script](https://gitlab.com/postgres-ai/database-lab/-/blob/master/scripts/create_zfs_snapshot.sh) as an example how this process can be automated.

## Step 4. Configure and launch the Database Lab server
Check the Database Lab configuration:

```bash
mkdir ~/.dblab
curl https://gitlab.com/postgres-ai/katacoda-tutorials/-/raw/master/database-lab-tutorial/assets/config.yml -o ~/.dblab/server.yml
cat ~/.dblab/server.yml
```

Launch your Database Lab instance:

```bash
docker run \
  --name dblab_server \
  --label dblab_control \
  --privileged \
  --publish 2345:2345 \
  --restart on-failure \
  --volume /var/run/docker.sock:/var/run/docker.sock \
  --volume /var/lib/dblab:/var/lib/dblab:rshared \
  --volume ~/.dblab/server.yml:/home/dblab/configs/config.yml \
  --detach \
  postgresai/dblab-server:0.4.3
```

Observe the logs:
```bash
docker logs dblab_server
```

Now we can check the status of the Database Lab server using a simple API call:
```bash
curl \
  --include \
  --request GET \
  --header 'Verification-Token: secret_token' \
  http://localhost:2345/status
```

## Step 5. Set up Database Lab client CLI
Install Database Lab client CLI:
```bash
curl https://gitlab.com/postgres-ai/database-lab/-/raw/master/scripts/cli_install.sh | bash
```

Initialize CLI and allow HTTP enabling `insecure` option in the config (not recommended for real-life use):

```bash
dblab init \
  --environment-id=tutorial \
  --url=http://127.0.0.1:2345 \
  --token=secret_token \
  --insecure
```

Check:
```bash
dblab instance status
```

Congratulations! Client CLI is configured and works properly.

## Step 6. Start cloning!
Request a new clone:
```bash
dblab clone create \
  --username dblab_user_1 \
  --password secret_password \
  --id my_first_clone
```

A few seconds later, if everything is configured correctly, you will see that the clone is ready to be used.

Now you can work with this clone using any PostgreSQL client – say, `psql`:
```bash
PGPASSWORD=secret_password \
  psql "host=127.0.0.1 port=6000 user=dblab_user_1 dbname=workshop" \
  -c '\l+'
```

To see the full information about the Database Lab instance, including the list of all currently available clones:
```bash
dblab instance status
```

Create more clones. Many more:
```bash
dblab clone create --username dblab_user_1 --password secret_password --id clone1
dblab clone create --username dblab_user_1 --password secret_password --id clone2
dblab clone create --username dblab_user_1 --password secret_password --id clone3
dblab clone create --username dblab_user_1 --password secret_password --id clone4
dblab clone create --username dblab_user_1 --password secret_password --id clone5
dblab clone create --username dblab_user_1 --password secret_password --id clone6
dblab clone create --username dblab_user_1 --password secret_password --id clone7
dblab clone create --username dblab_user_1 --password secret_password --id clone8
dblab clone create --username dblab_user_1 --password secret_password --id clone9
```

We have 10 independent databases now. But thanks to copy-on-write (CoW) and thin cloning, physically, the disk space consumed only once:
```bash
df -hT
```

Finally, let's manually delete all the clones:
```bash
dblab clone destroy my_first_clone
dblab clone destroy clone1
dblab clone destroy clone2
dblab clone destroy clone3
dblab clone destroy clone4
dblab clone destroy clone5
dblab clone destroy clone6
dblab clone destroy clone7
dblab clone destroy clone8
dblab clone destroy clone9
```

## Step 7. Feel independent
Request a new clone:
```bash
dblab clone create \
  --username dblab_user \
  --password secret_password \
  --id qa_clone
```

Show tables:
```bash
export QA_CONN_STR=$(dblab clone status qa_clone | jq -r '.db.connStr')

PGPASSWORD=secret_password \
psql "${QA_CONN_STR} dbname=workshop" \
  -c '\d+'
```

Drop table:
```bash
PGPASSWORD=secret_password \
psql "${QA_CONN_STR} dbname=workshop" \
  -c 'drop table pgbench_accounts'
```

Show tables:
```bash
PGPASSWORD=secret_password psql "${QA_CONN_STR} dbname=workshop" \
  -c '\d+'
```


Reset clone state:
```bash
dblab clone reset qa_clone
```

Show tables:
```bash
PGPASSWORD=secret_password psql "${QA_CONN_STR} dbname=workshop" \
  -c '\d+'
```

Delete the clone:

```bash
dblab clone destroy qa_clone
```

## Step 8. Easy check
An example of how database migrations (DB schema changes) can be automatically checked in CI/CD:

```bash
dblab clone create \
  --username dblab_user \
  --password secret_password \
  --id ci_clone
```

Wait a few seconds and run observer in the second terminal:
```bash
export CLONE_PASSWORD=secret_password

dblab clone observe \
  --interval-seconds 1 \
  --max-lock-duration-seconds 1 \
  --max-duration-seconds 300 \
  -f ci_clone
```

Return to the first terminal and export environment variables: 
```bash
export CI_CONN_STR=$( \
  dblab clone status ci_clone \
    | jq -r '.db.connStr' \
)
export CLONE_PASSWORD=secret_password
export PGPASSWORD=secret_password
```

Run easy migration:
```bash
psql "${CI_CONN_STR} dbname=workshop" \
  -c 'select 1'
```

Check results:
```bash
dblab clone observe-summary
```

Run a "bad" migration that implies dangerous locks:
```bash
psql "${CI_CONN_STR} dbname=workshop" \
  -c 'create table t1 as
         select i, random()::text as payload
         from generate_series(1, 5000000) i;
      alter table t1 alter column i set not null;'
```

Check results again:
```bash
dblab clone observe-summary
```

Send Ctrl+C to the running observer (the second terminal).

Return to the first terminal and delete the clone:

```bash
dblab clone destroy ci_clone
```

## Step 9. Feel full-size
Create a new clone for DBA:
```bash
dblab clone create \
  --username dblab_user \
  --password secret_password \
  --id dba_clone
```

Wait a few seconds and enter the running container: 
```bash
export DBA_CLONE_NAME=dblab_clone_$( \
  dblab clone status dba_clone \
  | jq -r '.db.port' \
)

docker exec -it ${DBA_CLONE_NAME} bash 
```

Connect to the database:
```bash
psql -U dblab_user -d workshop
```

Turn off the autovacuum:
```sql
alter system set autovacuum = off; 
select pg_reload_conf();
```

Update rows:
```sql
update pgbench_accounts set aid = aid where aid < 1000000;
```

Check the database size:
```
\l+ workshop
```

Vacuum the table:
```sql
vacuum full;
```

Check the database size again:
```
\l+ workshop
```

Quit from psql and the container:
```
\q
exit
```

Destroy the clone:
```
dblab clone destroy dba_clone
```

## Step 10. Multiple snapshots
Run initdb container:
```bash
docker start dblab_pg_initdb
```

Create a new table:
```bash
docker exec -it dblab_pg_initdb \
  psql -U postgres -d workshop \
    -c 'create table multiple (id integer)'
```

Show tables:
```bash
docker exec -it dblab_pg_initdb \
  psql -U postgres -d workshop \
    -c '\d+'
```

Stop initdb container:
```bash
docker stop dblab_pg_initdb
```

Create a new snapshot:
```bash
export DATA_STATE_AT="$(TZ=UTC date '+%Y%m%d%H%M%S')"
sudo zfs snapshot dblab_pool@multiple
sudo zfs set dblab:datastateat="${DATA_STATE_AT}" dblab_pool@multiple
```

Create a clone using new snapshot
```bash
dblab clone create \
  --username dblab_user \
  --password secret_password \
  --id multiple1
```

Check tables for the clone based on the new snapshot:
```
export MULTIPLE1_CONN_STR=$(dblab clone status multiple1 | jq -r '.db.connStr')

PGPASSWORD=secret_password psql "${MULTIPLE1_CONN_STR} dbname=workshop" \
  -c '\d+'
```

Create a clone using the old snapshot:
```bash
dblab clone create \
  --username dblab_user \
  --password secret_password \
  --id multiple2 \
  --snapshot-id dblab_pool@initdb
```

Check tables for the clone based on the old snapshot:
```
export MULTIPLE2_CONN_STR=$(dblab clone status multiple2 | jq -r '.db.connStr')

PGPASSWORD=secret_password psql "${MULTIPLE2_CONN_STR} dbname=workshop" \
  -c '\d+'
```

Destroy clones:
```bash
dblab clone destroy multiple1
dblab clone destroy multiple2
```

To learn more, sign in to Database Lab Platform: https://postgres.ai/console.

## Finish
That's it! Congratulations!

What's next?

- Sign in to https://postgres.ai/console/ and we'll show you SaaS part (GUI, access control, history, more)
- Check out the documentation https://postgres.ai/docs/
- Join our community chats:
    - Slack (English): https://database-lab-team-slack-invite.herokuapp.com/
    - Telegram (Russian): https://t.me/databaselabru


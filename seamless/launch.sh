#!/bin/bash

# Preparing your workspace, please wait (usually it takes ~2-3 minutes)...

sudo docker run -p 5432:5432 --name demo1 --detach -it postgresai/seamless:latest
sudo docker exec -it demo1 bash

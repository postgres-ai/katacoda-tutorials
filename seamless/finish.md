That's it! Congratulations!

What's next?

### Let's keep in touch
- https://postgres.ai
- nik@postgres.ai
- Twitter: [@samokhvalov](https://twitter.com/samokhvalov)
- LinkedIn: https://www.linkedin.com/in/samokhvalov/


### Learn more about development methodologies transformation and automatization of database-related tasks
- Check out what my team and I are doing to help engineers boost their work with PostgreSQL databases: https://Postgres.ai
- Sign in, and our team will contact you to make a 30-minute demonstration call
- We would love to share our ideas on changing the development methodologies, automatization of database-related tasks, and hear your feedback
- Join:
    - [Community Slack (English)](https://database-lab-team-slack-invite.herokuapp.com/) (a live Joe bot is in the [#joe-bot-demo](https://database-lab-team.slack.com/archives/CTL5BB30R) channel)
    - [Telegram (Russian)](https://t.me/databaselabru)

# Preparation
Prepate an Ubuntu machine (any modern version), with Docker installed. (How to quickly install Docker on Ubuntu: https://postgres.ai/docs/tutorials/database-lab-tutorial#install-docker).

Then launch a Docker container and "bash inside" it:
```bash
sudo docker run -p 5432:5432 --name demo1 --detach -it postgresai/seamless:latest
sudo docker exec -it demo1 bash
```

# Step 1
We will use database `demo1`. Username and password are not required.

Check that you have PostgreSQL and can connect using `psql`:

```bash
psql demo1 -c 'select now()'
```

Before proceeding, ensure that extension `pg_stat_statements` is already installed:

```bash
psql demo1 -c 'select * from pg_stat_statements limit 1' # if it doesn't work, just WAIT a minute!
```

If it's not, **wait another minute or two**.

# Step 2. Check currently processing queries

## Using psql

```bash
psql demo1 -c 'select * from pg_stat_activity'
```

## Using pgCenter (alteady installed)

Use pgCenter to to see currently executed queries:

```bash
pgcenter top demo1
```

To exit any time, press `q` (works both for psql's wide output and for pgCenter).

# Step 3. Macro-analysis: use `pg_stat_statements`

Get the "Top-5 by `total_exec_time`":

- lanuch psql:

    ```sql
    psql demo1
    ```

- and run in it:

    ```sql
    select * from pg_stat_statements order by total_exec_time desc limit 5;
    ```


Pay attention to metrics:

- `calls`
- `total_exec_time`
- `min_exec_time`, `max_exec_time`, `mean_exec_time`
- `shared_blks_{read,hit}`

How would you optimize?

# Step 4

Use `EXPLAIN` to perform micro-analysis. (Would you prefer doing it somewhere else if it was production?)

- First, do `EXPLAIN` without execution: 

    ```sql
    explain select count(*) from pgbench_history where mtime::date = '2020-05-26';
    ```

- Next, perform a full-fledged micro-analysis using `EXPLAIN (ANALYZE, BUFFERS)`, but be ready to wait:

    ```sql
    explain (analyze, buffers) select count(*) from pgbench_history where mtime::date = '2020-05-26';
    ```

# Step 5
Let's optimize the query!

Run (in psql):

```sql
create index concurrently i1 on pgbench_history((mtime::date));
```

(Would you prefer checking it in a separate environment first?)

Do not forget to re-gather statistics because we have added an index on expression:

```sql
analyze pgbench_history;
```


# Step 6

Perform macro-analysis again, to ensure that our optimization helped.

Reset pg_stat_statements stats:

```sql
select pg_stat_statements_reset();
```

> Wait ~30-60 seconds before proceeding – we want to collect some stats.

Check the "Top-5 by `total_exec_time`" again:

```sql
select * from pg_stat_statements order by total_exec_time desc limit 5;
```

How do you interpret results? What could go wrong with our optimization?


# Step 7

Do micro-analysis again, but this time check two different values and see if there is any difference:

- Check for `2021-03-01`:
    ```sql
    explain (buffers, analyze) select count(*) from pgbench_history where mtime::date = '2021-03-01';
    ```

- Check for `2000-01-01` (some day in the past):
    ```sql
    explain (buffers, analyze) select count(*) from pgbench_history where mtime::date = '2000-01-01';
    ```

Do you see the difference?

What is the reason behind it?

# Step 8

Let's get rid of the existing data in `pgbench_history` and then re-analyze.

```sql
truncate pgbench_history;
```

Reset pg_stat_statements stats:

```sql
select pg_stat_statements_reset();
```

And after a moment of two, check the "Top-5 by `total_exec_time`"` again:

```sql
select * from pg_stat_statements order by total_exec_time desc limit 5;
```


# Step 9
Visualize EXPLAIN data right from psql. For this, let's use a small utility called [plan-exporter](https://github.com/agneum/plan-exporter) (already installed).

Make plan-exporter listen the output in your psql:

```
\o | plan-exporter
```

Now run any `EXPLAIN`. Some arbitrary example:

```sql
explain (analyze, buffers)
select *
from pgbench_accounts
join pgbench_branches using (bid)
where aid < 1000
order by bbalance desc
limit 20;
```

You'll see both the traditional output of `EXPLAIN` and additional message suggesting to submit the data to `explain.depesz.com`. To agree, enter the following:

```
\qecho Y
```

You should get the link to EXPLAIN data visualization at `explain.depesz.com`. Check it out.

Now, let's do another visualization – this time, using `explain.dalibo.com`. Switch to this service:

```

\o | plan-exporter --target=dalibo
```

Again, run some`EXPLAIN`:

```
explain (analyze, buffers)
select *
from pgbench_accounts
join pgbench_branches using (bid)
where aid < 1000
order by bbalance desc
limit 20;
```

And as before, agree with suggestion to post the data:

```
\qecho Y
```

Check out the new visualization. Pay attention to minimap on the left upper corner, and BUFFERS data visualization.


# Step 10

Try setting up the Database Lab Engine and work with thin clones:

- Docs: https://postgres.ai/docs
- Katacoda step-by-step tutorial: https://www.katacoda.com/postgres-ai/scenarios/database-lab-tutorial
- Communnity demo video (Russian): [part 1](https://www.youtube.com/watch?v=-ltBmGRrzFY&list=PL6sRAkPwcKNmlGlaZjSZHbji_IvoBOHVz&index=7), [part 2](https://www.youtube.com/watch?v=t3vfGTcG91o&list=PL6sRAkPwcKNmlGlaZjSZHbji_IvoBOHVz&index=8)
- [Community Slack (English)](https://database-lab-team-slack-invite.herokuapp.com/) (a live Joe bot is in the [#joe-bot-demo](https://database-lab-team.slack.com/archives/CTL5BB30R) channel)
- [Telegram (Russian)](https://t.me/databaselabru)

---

That's it! Congratulations!

What's next?

### Let's keep in touch
- https://postgres.ai
- nik@postgres.ai
- Twitter: [@samokhvalov](https://twitter.com/samokhvalov)
- LinkedIn: https://www.linkedin.com/in/samokhvalov/


### Learn more about development methodologies transformation and automatization of database-related tasks
- Check out what my team and I are doing to help engineers boost their work with PostgreSQL databases: https://Postgres.ai
- Sign in, and our team will contact you to make a 30-minute demonstration call
- We would love to share our ideas on changing the development methodologies, automatization of database-related tasks, and hear your feedback
- Join:
    - [Community Slack (English)](https://database-lab-team-slack-invite.herokuapp.com/) (a live Joe bot is in the [#joe-bot-demo](https://database-lab-team.slack.com/archives/CTL5BB30R) channel)
    - [Telegram (Russian)](https://t.me/databaselabru)

